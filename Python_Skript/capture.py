
import ifaces
import subprocess

def capture_packets():

    '''
    Specify how packet capture shall happen
    '''

    print("\nChoose capture setting:\n")
    print("The capture sequence can be stop after...")
    print("...a certain number of packets [p]")
    print("...a certain time-interval in seconds [t]\n")

    setting = input("Choose your criterion [t|p]: \n")
    
    if setting == "p":
        setting_p = input("How many packets shall be captured?\n")
        num_of_packets = "-c " + str(setting_p)
        subprocess.call(["dumpcap", ifaces.interface, num_of_packets])
    
    elif setting == "t":
        setting_t = input("How many seconds shall the capture proceed? \n")
        time_to_capture = "-a"
        time_to_capture2 = "duration:" + str(setting_t)
        subprocess.call(["dumpcap", ifaces.interface, time_to_capture, time_to_capture2])

    else:
        print("\nYour input is incorret. Please choose a time-interval or an amount of packets to be captured. \n")
    




def main():
    capture_packets()


if __name__ == '__main__':
    main()
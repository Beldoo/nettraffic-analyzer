import os
import subprocess


## sets path to the location where the captured data shall be saved to (the folder where the app lives in)
path_to_json = os.getcwd()


def clear_target_dir():
    
    '''
    asks user, if all .pcapng-files in /tmp can be deleted
    '''

    allow = input("Can all existing .pcacap/-pcap-files be deleted? [y|n] ")

    if allow == "y":
        subprocess.call(["rm /tmp/*.pcapng"], shell=True)
        subprocess.call(["rm /tmp/*.pcap"], shell=True)
    else:
        print("Deletion of .pcapng-files aborted.")
        pass



def convert():

    '''
    This function converts the .pcapng file into a plain .txt file
    '''
    
    subprocess.call(["tshark -V -r /tmp/*.pcapng -T json > {0}/capture.json".format(path_to_json)], shell=True)

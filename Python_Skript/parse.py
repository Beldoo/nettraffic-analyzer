import json
import os

## This section provides the exact path to the file containing the captured nettraffic data
path_to_json = os.getcwd()
json_file = path_to_json + "/capture.json"


def parse_all():

    ## load data from .json-file:
    json_data = json.loads(open(json_file).read())
    num_pack = len(json_data)
    
    ## create a dict in relation to the number of packets within the .json-file:
    all_packets_protocols = {}
    for ii in range(len(json_data)):
        all_packets_protocols['packet_' + str(ii)] = {'timestamp':None, 'protocols':[]}


    ## find all protocol layers in packet:

    ## loop through the .json-file:
    c = 0
    for x in json_data:
        for key in x.keys():
            
            ## all important inf. is in "_source"
            if key == '_source':
                neu = x[key]['layers']  # subsection 'layers' contains exact time-stamp & used protocols
                for ii in neu.keys():
                    if 'frame' in ii:
                        pass  
                    else:
                        ## we are only interested in the protocols used in this package
                        all_packets_protocols['packet_' + str(c)]['protocols'].append(ii)

            if key == '_source':
                ## here we filter out the time-stamp (within the 'frame' section):
                time = x[key]['layers']['frame']['frame.time']
                all_packets_protocols['packet_' + str(c)]['timestamp'] = time

        c += 1


    print("Number of captured packets: {0}".format(num_pack))


    ## output options ##
    inspect = input("Wanna see all packtets? [y|n]")
    
    if inspect == 'y':
        print("\n")
        for ii in range(num_pack):
            print("Packet" + str(ii))
            print('Timestamp: ', all_packets_protocols[ 'packet_' + str(ii) ]['timestamp'])
            print('Protocols' + ': ', all_packets_protocols[ 'packet_' + str(ii) ]['protocols'], "\n")



def main():
    parse_all()


if __name__ == '__main__':
    main()
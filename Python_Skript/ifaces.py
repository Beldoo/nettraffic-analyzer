
import psutil
from termcolor import colored, cprint

global interface
interface = "default_string_which_is_unlikey_to_be_found_on_your_system_!"

found_ifaces = psutil.net_if_addrs()


def find_interfaces():

    '''
    This function gets all network interfaces
    '''

    print("\nList of network interfaces:\n")
    for iface in found_ifaces:
        #print(addrs.keys())
        print(iface)



def choose_interface():

    '''
    This function lets the user choose interface which will be used for traffic analysis
    '''
    
    ## the interface variable will be used in other modules
    global interface
    
    interface = input("\nInterface: ")

    if interface not in found_ifaces:
        cprint("\n\"{0}\" not available. Please choose one of the existing interface from the list.".format(interface), 'red', attrs=['bold'])
        choose_interface()
    else:
        cprint("\nInterface set successfully.\n", 'green', attrs=['bold'])


def main():
    find_interfaces()
    choose_interface()



if __name__ == '__main__':
    main()

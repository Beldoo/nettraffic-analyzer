#import MySQLdb as MySQL
import mysql.connector
from mysql.connector import MySQLConnection, Error


def test_connection():

    '''
    This function tests if a connection can be accomplished to the specifies database
    '''

    try:
        con = mysql.connector.connect(host='localhost', 
                                    database='netdb',
                                    user='root', 
                                    password='test',  
                                    port = 3306, 
                                    charset='utf8')
        if con.is_connected():
            print("SUCCESS CONNECTION")

    except Error as e:
        print(e)
        print("KEINE VERBINDUNG ZUM SERVER!")




def upload_data_db_base(input_value):

    try:
        con = mysql.connector.connect(host='localhost', 
                                      database='netdb',
                                      user='root', 
                                      password='test',  
                                      port = 3306, 
                                      charset='utf8')
        if con.is_connected():
            #print("SUCCESS CONNECTION")
            pass

    except Error as e:
        print(e)
        print("KEINE VERBINDUNG ZUM SERVER!")

    try:
        cur = con.cursor()
        cur.execute(input_value)
        con.commit()
        con.close()
        print('SUCCESS APPLYING SQL STATEMENT!')        
    except:
        print('ERROR APPLYING SQL STATEMENT!')



def insert_in_db(value, column):

    '''
    This function lets you insert values in your table "packets"
    '''

    sql_insert_part_1 = "insert into packets"
    sql_insert_part_2 = "VALUES ("
    sql_insert_part_3 = ")"
    upload_data_db_base(sql_insert_part_1 + str(column) + sql_insert_part_2 + str(value) + sql_insert_part_3)



def delete_all_db():

    '''
    This function deletes all values from table "packets"
    '''

    sql_insert_part = "delete from packets"
    upload_data_db_base(sql_insert_part)





def main():
    test_connection()
    insert_in_db(5500, "(ip)")   # welcher WERT soll in welche SPALTE
    #delete_all_db()
    

    #for ii in range(11):
    #    upload_data_db(sql_insert_part_1 + str(ii) + sql_insert_part_2)



if __name__ == '__main__':
    main()


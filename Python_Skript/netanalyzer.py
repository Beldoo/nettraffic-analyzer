
## load all python modules:
import convert
import ifaces
import capture
import parse

import subprocess


def clear_target_dir():
    
    '''
    asks user, if all .pcapng-files in /tmp can be deleted
    '''

    allow = input("Can all .pcacap-files be deleted? [y|n] ")
    if allow == "y":
        subprocess.call(["rm /tmp/*.pcapng"], shell=True)
        subprocess.call(["rm /tmp/*.pcap"], shell=True)
    else:
        print("Deletion of .pcapng-files aborted.")
        pass



def main():
    ifaces.find_interfaces()
    ifaces.choose_interface()
    clear_target_dir()
    capture.capture_packets()
    convert.convert()
    parse.parse_all()


if __name__ == '__main__':
    main()
    
# Tool for capturing, analyzing and manipulating network traffic

This tool lets you capture nettraffic from a network interface you choose. At the moment, you can check the time-stamp and the protocol-layers of each packet. Further feature are going to be implemented soon.

## Required Modules:

- termcolor
- pyutil

